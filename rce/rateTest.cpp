//////////////////////////////////////////////////////////////////////////////
// This file is part of 'Example RCE Project'.
// It is subject to the license terms in the LICENSE.txt file found in the 
// top-level directory of this distribution and at: 
//    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
// No part of 'Example RCE Project', including this file, 
// may be copied, modified, propagated, or distributed except according to 
// the terms contained in the LICENSE.txt file.
//////////////////////////////////////////////////////////////////////////////
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <limits.h>
#include <termios.h>
#include <thread>

#include <AxiStreamDma.h>
#include <Pgp2bAxiReg.h>

#define SOF_MASK  0x2 // Start of Frame
#define EOF_MASK  0x0 // End of frame without errors

class PgpRunData {
public:
  PgpRunData():count(0),total(0) {};
  int fd;
  unsigned long count;
  unsigned long total;
};

class PgpSender {
public:
  PgpSender(unsigned int lane,unsigned int vc,unsigned int size,unsigned int loopback=0):pgpLane(lane),virtualChannel(vc),dataSize(size),loopbackMode(loopback) {
    if ( (fd = open("/dev/axi_stream_dma_0",O_WRONLY|O_NONBLOCK)) < 0 ) {
      std::cerr  << "Unable to open AXIS dma device\n";
      throw;
    }
    PgpRegister pgp(pgpLane);
    pgp.reset();
    pgp.writeReg("Loopback",loopbackMode);
  };
  PgpRunData runData;
  void operator()()  {
    taskLoop();
  }
private:
  void taskLoop() {
    fd_set          fds;
    struct timeval  timeout;
    uint32_t        error;
    int             ret;
    uint32_t *      data;
    std::cout << "Starting write thread: lane= " << pgpLane << ", vc= "<< virtualChannel << ", loopback= "<< loopbackMode << ", dataSize= " <<dataSize << std::endl;
    unsigned int tdestMask= ( (pgpLane << 4) | virtualChannel );
    PgpRegister pgp(pgpLane);
    data = (uint32_t *)malloc(sizeof(uint32_t)*dataSize);
    error = 0;
    while (error == 0) {
      // Setup fds for select call
      FD_ZERO(&fds);
      FD_SET(fd,&fds);
      // Wait for write ready
      timeout.tv_sec=5;
      timeout.tv_usec=0;
      ret = select(fd+1,NULL,&fds,NULL,&timeout);
      if ( ret <= 0 ) {
	std::cout << "Write timeout. Ret=" << ret << std::endl;
         error++;
      }
      else {
	ret = axisWrite(fd, data, dataSize*sizeof(uint32_t), SOF_MASK, EOF_MASK, tdestMask);
	if ( ret <= 0 ) {
	  std::cout << "Write Error: " << std::dec << ret << std::endl;
	  error++;
	}
	else {
            runData.count++;
            runData.total+= ret;
	}
      }
    }
   free(data);
  };
  unsigned int pgpLane;
  unsigned int virtualChannel;
  unsigned int dataSize;
  unsigned int *data;
  unsigned int loopbackMode;
  int32_t fd;
};

class PgpReceiver {
public:
  PgpReceiver(unsigned int lane,unsigned int size,unsigned int loopback=0):pgpLane(lane),dataSize(size),loopbackMode(loopback){
    if ( (fd = open("/dev/axi_stream_dma_0",O_RDONLY|O_NONBLOCK)) < 0 ) {
      std::cerr  << "Unable to open AXIS dma device\n";
      
      throw;
    }
    // flush read buffer
    PgpRegister pgp(pgpLane);
    pgp.reset();
    pgp.writeReg("Loopback",loopbackMode);
    int32_t flush;
    while( (flush = axisRead(fd,NULL,(2*dataSize*sizeof(uint32_t)),NULL,NULL,NULL)) !=0 ){
      usleep(1000);
   }   
    
  };
  PgpRunData runData;
  void operator()()  {
    taskLoop();
  }
private:
  void taskLoop() {
    fd_set          fds;
    struct timeval  timeout;
    uint32_t        error;
    int             ret;
    uint32_t *      data;
    uint32_t        maxSize;
    uint32_t        firstUser;
    uint32_t        lastUser;
    uint32_t        axisDest;   

    maxSize = dataSize*2;
    data = (uint32_t *)malloc(sizeof(uint32_t)*maxSize);
    error = 0;
    std::cout << "Starting read thread: lane= " << pgpLane << ", loopback= "<< loopbackMode << ", dataSize= " <<dataSize << std::endl;
    
    while (error == 0) {
      // Setup fds for select call
      FD_ZERO(&fds);
      FD_SET(fd,&fds);
      // Wait for read ready
      timeout.tv_sec=5;
      timeout.tv_usec=0;
      ret = select(fd+1,&fds,NULL,NULL,&timeout);
      if ( ret <= 0 ) {
	std::cout << "Read timeout. Ret=" << ret << std::endl;
         error++;
      }
      else {
	ret = axisRead(fd, data, maxSize*sizeof(uint32_t), &firstUser, &lastUser, &axisDest);
	if ( ret != dataSize*sizeof(uint32_t) ) {
	  std::cout << "Read Error. Ret=" << std::dec << ret << std::endl;
            error++;
         }
         else {
            runData.count++;
            runData.total += ret;
         }
      }
    }
   free (data);
  };
private:
  unsigned int virtualChannel;
  unsigned int pgpLane;
  unsigned int dataSize;
  unsigned int loopbackMode;
  unsigned int *data;
  int32_t fd;
};

void usage() {
  std::cerr << "usage: rateTest [-m dataSize] [-l loopback] [-v virtualChannel] [-s (0/6)] -r [(0/6)]" << std::endl;
  exit(0);
}

int main(int argc,char* argv[]) {
  unsigned dataSize=0x400; // default data size in words 
  unsigned int vc=0; // virtual channel default
  unsigned int recvLane=0;
  unsigned int senderLane=0;
  unsigned int loopback=2; // internal
  bool startRecv=false;
  bool startSender=false;
  int c;
  while ((c = getopt (argc, argv, "m:l:v:r:s:")) != -1) {
    switch(c) {
    case 'm':
      dataSize=std::stoi(optarg);
      break;
    case 'v':
      vc=std::stoi(optarg);
      break;
    case 'l':
      loopback=std::stoi(optarg);
      break;   
    case 'r':
      recvLane=std::stoi(optarg);
      startRecv=true;
      break;
    case 's':
      senderLane=std::stoi(optarg);
      startSender=true;
      break;
    default:
      usage();
      break;
    }
  }
  if(dataSize<1 || dataSize>0x400) {
    usage();
  }
  if(loopback>4) {
    usage();
  }
  if(vc>4) {
    usage();
  }
if(!(recvLane==0 || recvLane==6)) {
    usage();
 }
if(!(senderLane==0 || senderLane==6)) {
    usage();
  }

if(!startSender &&  !startRecv) {
  startSender=true;
  startRecv=true;
 }
  RceRegister rce;
  std::thread receiverTask;
  std::thread senderTask;
  std::cout << "Build Stamp: "<< rce.buildString() << std::endl << std::endl;
  PgpSender sender(senderLane,vc,dataSize,loopback);
  PgpReceiver receiver(recvLane,dataSize,loopback);
if(startRecv) {
  receiverTask= std::thread(std::ref(receiver));
  sleep(1);
 }
if(startSender) {
   senderTask= std::thread(std::ref(sender));
 }
  uint32_t previous=0;
  while(1) {
    double rate=0.;
    uint32_t  rx=receiver.runData.total; // total bytes received
    if(previous>0) rate=(rx-previous)/1024./1024.;
    std::cout << "Tx= " <<  sender.runData.total  << ", Rx= " << rx << ", rate= " << rate<<  "Gb/s" << std::endl;
    previous=rx;
    sleep(1);
  }
  if(startSender) senderTask.join();
  if(startRecv) receiverTask.join();
}




