//-----------------------------------------------------------------------------
// File          : PrbsData.cpp
// Author        : Ryan Herbst <rherbst@slac.stanford.edu>
// Created       : 10/27/2014
// Project       : LBNE DAQ
//-----------------------------------------------------------------------------
// Description :
// Process blocks of PRBS data received from DMA engine. Used to verify
// DMA operation and memory mapping.
//-----------------------------------------------------------------------------
// This file is part of 'Example RCE Project'.
// It is subject to the license terms in the LICENSE.txt file found in the 
// top-level directory of this distribution and at: 
//    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
// No part of 'Example RCE Project', including this file, 
// may be copied, modified, propagated, or distributed except according to 
// the terms contained in the LICENSE.txt file.
// Proprietary and confidential to SLAC.
//-----------------------------------------------------------------------------
// Modification history :
// 10/27/2014: created
//-----------------------------------------------------------------------------
#include <PrbsData.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

PrbsData::PrbsData (uint32_t width, uint32_t tapCnt, ...) {
   va_list a_list;
   uint32_t   x;

   _sequence = 0;
   _width    = width;
   _tapCnt   = tapCnt;

   _taps = (uint32_t *)malloc(sizeof(uint32_t)*tapCnt);

   va_start(a_list,tapCnt);
   for(x=0; x < tapCnt; x++) _taps[x] = va_arg(a_list,uint32_t);
   va_end(a_list);
}

PrbsData::~PrbsData() {
   free(_taps);
}

bool PrbsData::processData ( const void *data, uint32_t size ) {
   uint32_t   eventLength;
   uint32_t   expected;
   uint32_t   got;
   uint32_t   word;
   uint32_t   min;

   uint32_t * data32 = (uint32_t *)data;
   uint16_t * data16 = (uint16_t *)data;

   if ( _width == 16 ) {
      expected    = data16[0];
      eventLength = (data16[1] * 2) + 2;
      min         = 6;
   }
   else if ( _width == 32 ) {
      expected    = data32[0];
      eventLength = (data32[1] * 4) + 4;
      min         = 12;
   }
   else return(false);

   if (size < min || eventLength != size) {
      fprintf(stderr,"Bad size. exp=%i, min=%i, got=%i\n",eventLength,min,size);
      return(false);
   }

   if ( _sequence != 0 && _sequence != expected ) {
      fprintf(stderr,"Bad Sequence. exp=%i, got=%i\n",_sequence,expected);
      _sequence = expected + 1;
      return(false);
   }
   _sequence = expected + 1;

   for (word = 2; word < size/(_width/8); word++) {
      expected = flfsr(expected);

      if      ( _width == 16 ) got = data16[word];
      else if ( _width == 32 ) got = data32[word];

      if (expected != got) {
         fprintf(stderr,"Bad value at index %i. exp=0x%x, got=0x%x\n",word,expected,got);
         return(false);
      }
   }
   return(true);
}

uint32_t PrbsData::flfsr(uint32_t input) {
   uint32_t bit = 0;
   uint32_t x;

   for (x=0; x < _tapCnt; x++) bit = bit ^ (input >> _taps[x]);

   bit = bit & 1;

   //return (input >> 1) | (bit << _width);
   return ((input << 1) | bit);
}

