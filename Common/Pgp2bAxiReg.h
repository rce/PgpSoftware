//-----------------------------------------------------------------------------
// This file is part of 'Example RCE Project'.
// It is subject to the license terms in the LICENSE.txt file found in the 
// top-level directory of this distribution and at: 
//    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
// No part of 'Example RCE Project', including this file, 
// may be copied, modified, propagated, or distributed except according to 
// the terms contained in the LICENSE.txt file.
// Proprietary and confidential to SLAC.
//-----------------------------------------------------------------------------

#ifndef __PGP2B_REG_H__
#define __PGP2B_REG_H__

#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <unistd.h>

#include <map>
#include <string>
#include <iostream>
#include <iomanip>

class RegisterAccess {
 public:
 RegisterAccess():reg(-1) {
    if ( (reg = open("/dev/mem",O_RDWR|O_SYNC)) < 0 ) {
      close(reg);
      throw;
    }
  }
  ~RegisterAccess() {
    if(reg>=0) close(reg);
  }
  void dumpRegs() {
    for(std::map<std::string,AddrMap>::iterator it=RegisterMap.begin();it!=RegisterMap.end();++it) {
      unsigned int dim= it->second.dimension;
      std::string index;
      unsigned int width=22;
      for(unsigned int i=0;i<dim;i++) {
	if(dim>1)
	  index="["+std::to_string(i)+"]";
	else
	  index="";
	std::string name= it->first + index;
	unsigned int val= readReg(it->first,i);
	std::cout << std::left <<std::setw(width) <<name <<" :  " <<std::setw(12) << std::left<<std::showbase<<std::hex<< val  << std::dec << val  <<std::endl;	
      }
    }
  } 
  void writeReg(std::string regname,uint32_t value,uint32_t offset=0) {
    std::map<std::string,AddrMap>::iterator it;
    it=RegisterMap.find(regname); 
    (it->second.addr)[offset]=value;
  }
  uint32_t readReg(std::string regname,uint32_t offset=0) {
    std::map<std::string,AddrMap>::iterator it;
    it=RegisterMap.find(regname);
    return ((it->second.addr)[offset]>>it->second.offset)&it->second.mask;
  }
  uint32_t *getAddr(std::string regname,uint32_t offset=0) {
    std::map<std::string,AddrMap>::iterator it;
    it=RegisterMap.find(regname);
    return &((it->second.addr)[offset]);   
  }

 protected:
   typedef struct AddrMap_ {
     uint32_t *addr;
     unsigned int offset;
     unsigned int dimension;
     unsigned int mask;
   } AddrMap;
   std::map<std::string,AddrMap> RegisterMap;
   int32_t reg;  
};


class RceRegister:public RegisterAccess {

 public:
 RceRegister(): RegisterAccess() {
    uint32_t   *addr;
    uint32_t   *addr2;
    addr = (uint32_t *)mmap(NULL, 0x10000, (PROT_READ|PROT_WRITE), (MAP_SHARED), reg, 0x80000000);  
    addr2= (uint32_t *)mmap(NULL, 0x10000, (PROT_READ|PROT_WRITE), (MAP_SHARED), reg, 0x84000000);  
    RegisterMap["FpgaVersion"]={addr,0,1,0xffffffff};
    RegisterMap["RceVersion"]={addr+2,0,1,0xffffffff};
    RegisterMap["DeviceDna"]={addr+8,0,2,0xffffffff};
    RegisterMap["EFuseValue"]={addr+12,0,1,0xffffffff};
    RegisterMap["EthMode"]={addr+13,0,1,0xffffffff};
    RegisterMap["Heartbeat"]={addr+14,0,1,0xffffffff};
    RegisterMap["SerialNumber"]={addr2+0x50,0,1,0xffffffff};
    RegisterMap["Cluster"]={addr2+0x51,0,1,0xffffffff};
    RegisterMap["AtcaSlot"]={addr2+0x51,16,1,0xff};
    RegisterMap["CobBay"]={addr2+0x51,8,1,0xff};
    RegisterMap["CobElement"]={addr2+51,0,1,0xff};
    RegisterMap["BuildString"]={addr+0x400,0,64,0xff};
  }
  std::string buildString() {
    uint32_t BuildStamp[64];
    for(unsigned int i=0;i<64;i++)
      BuildStamp[i]=getAddr("BuildString")[i];
    return std::string((char *)BuildStamp);
  }
};


class PgpRegister: public RegisterAccess {
// Address Map, offset from base
/*
typedef struct Pgp2bAxiReg_ {
   //PciApp.vhd  
   uint32_t CountReset;             // Address Offset = 0x00
   uint32_t ResetRx;                // Address Offset = 0x04
   uint32_t Flush;                  // Address Offset = 0x08
   uint32_t Loopback;               // Address Offset = 0x0C
   uint32_t LocData;                // Address Offset = 0x10
   uint32_t AutoStatus;             // Address Offset = 0x14
   uint32_t UNUSED_A[2];            // Address Offset = 0x1C:0x18
   uint32_t Status;                 // Address Offset = 0x20
   // Note: 
   // Status.BIT0       = "RxPhyReady"
   // Status.BIT1       = "TxPhyReady"
   // Status.BIT2       = "RxLocalLinkReady"
   // Status.BIT3       = "RxRemLinkReady"
   // Status.BIT4       = "TxLinkReady"
   // Status.BIT[9:8]   = "RxLinkPolarity"
   // Status.BIT[15:12] = "RxRemPause"
   // Status.BIT[19:16] = "TxLocPause"
   // Status.BIT[23:20] = "RxRemOverflow"
   // Status.BIT[27:24] = "TxLocOverflow"
   uint32_t RxRemLinkData;          // Address Offset = 0x24
   uint32_t RxCellErrorCount;       // Address Offset = 0x28
   uint32_t RxLinkDownCount;        // Address Offset = 0x2C
   uint32_t RxLinkErrorCount;       // Address Offset = 0x30
   uint32_t RxRemOverflowCount[4];  // Address Offset = 0x40:34
   uint32_t RxFrameErrorCount;      // Address Offset = 0x44
   uint32_t RxFrameCount;           // Address Offset = 0x48
   uint32_t TxLocOverflowCount[4];  // Address Offset = 0x58:0x4C
   uint32_t TxFrameErrorCount;      // Address Offset = 0x5C
   uint32_t TxFrameCount;           // Address Offset = 0x60
   uint32_t RxClkFreq;              // Address Offset = 0x64
   uint32_t TxClkFreq;              // Address Offset = 0x68
   uint32_t UNUSUED_B[1];           // Address Offset = 0x6C
   uint32_t LastTxOpCode;           // Address Offset = 0x70
   uint32_t LastRxOpCode;           // Address Offset = 0x74
   uint32_t TxOpCodeCount;          // Address Offset = 0x78
   uint32_t RxOpCodeCount;          // Address Offset = 0x7C
} Pgp2bAxiReg;
*/
 public:
 void reset()  {
   writeReg("Flush",1);
   writeReg("ResetRx",1);
   writeReg("CountReset",1);
   writeReg("Flush",0);
   writeReg("ResetRx",0);
   writeReg("CountReset",0);
 }; 

 PgpRegister(unsigned int lane):RegisterAccess() {
   addr=(uint32_t *)mmap(NULL, 0x100, (PROT_READ|PROT_WRITE), (MAP_SHARED), reg, (0xA0000000 + lane*0x10000) );
   RegisterMap["CountReset"]={addr,0,1,0xffffffff};
   RegisterMap["ResetRx"]={addr+1,0,1,0xffffffff};   
   RegisterMap["Flush"]={addr+2,0,1,0xffffffff};
   RegisterMap["Loopback"]={addr+3,0,1,0xffffffff};  
   RegisterMap["LocData"]={addr+4,0,1,0xffffffff};   
   RegisterMap["AutoStatus"]={addr+5,0,1,0xffffffff};  
   RegisterMap["Status"]={addr+8,0,1,0xffffffff};
   RegisterMap["RxPhyReady"]={addr+8,0,1,1};
   RegisterMap["TxPhyReady"]={addr+8,1,1,1};
   RegisterMap["RxLocalLinkReadyStatus"]={addr+8,2,1,1};
   RegisterMap["RxRemLinkReady"]={addr+8,3,1,1};
   RegisterMap["TxLinkReady"]={addr+8,4,1,1};
   RegisterMap["RxLinkPolarity"]={addr+8,8,1,3};
   RegisterMap["RxRemPaus"]={addr+8,12,1,7};
   RegisterMap["TxLocPause"]={addr+8,16,1,7};
   RegisterMap["RxRemOverflow"]={addr+8,23,1,7};
   RegisterMap["TxRemOverflow"]={addr+8,24,1,7};
   RegisterMap["RxRemLinkData"]={addr+9,0,1,0xffffffff};
   RegisterMap["RxCellErrorCount"]={addr+10,0,1,0xffffffff};
   RegisterMap["RxLinkDownCount"]={addr+11,0,1,0xffffffff};
   RegisterMap["RxLinkErrorCount"]={addr+12,0,1,0xffffffff};
   RegisterMap["RxRemOverflowCount"]={addr+13,0,4,0xffffffff};
   RegisterMap["RxFrameErrorCount"]={addr+17,0,1,0xffffffff};
   RegisterMap["RxFrameCount"]={addr+18,0,1,0xffffffff};
   RegisterMap["TxLocOverflowCount"]={addr+19,0,4,0xffffffff};
   RegisterMap["TxFrameErrorCount"]={addr+23,0,1,0xffffffff};
   RegisterMap["TxFrameCount"]={addr+24,0,1,0xffffffff};
   RegisterMap["RxClkFreq"]={addr+25,0,1,0xffffffff};
   RegisterMap["TxClkFreq"]={addr+26,0,1,0xffffffff};
   RegisterMap["LastTxOpCode"]={addr+28,0,1,0xffffffff};
   RegisterMap["LastRxOpCode"]={addr+29,0,1,0xffffffff};
   RegisterMap["TxOpCodeCount"]={addr+30,0,1,0xffffffff};
   RegisterMap["RxOpCodeCount"]={addr+31,0,1,0xffffffff};    
 }
  void writeReg(std::string regname,uint32_t value,uint32_t offset=0) {
    std::map<std::string,AddrMap>::iterator it;
    it=RegisterMap.find(regname); 
    (it->second.addr)[offset]=value;
  }
  
 private:
  uint32_t *addr;
  uint32_t *addr2;
  
};

#endif
