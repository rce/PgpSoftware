# Variables
BIN        := $(PWD)/bin
DEF_CFLAGS := -Wall  -I$(PWD)/Common 
DEF_LFLAGS :=  -lpthread -lrt 

# RCE Build
   DEF      := -DARM
   CFLAGS   := -std=c++11 -g $(DEF_CFLAGS) --sysroot $(RCE_SDK_ROOT)/ArchLinux -I$(RCE_SDK_ROOT)/ArchLinux/usr/include -g
   LFLAGS   := $(DEF_LFLAGS) -L$(RCE_SDK_ROOT)/ArchLinux/usr/lib -lstdc++
   EXT_TARS := 
   CLN      := 
   UTL_DIR  := $(PWD)/rce
   OBJ      := $(PWD)/.rce_obj
   CC       := arm-unknown-linux-gnueabihf-cc
# Common Sources
GEN_DIR := $(PWD)/Common
GEN_SRC := $(wildcard $(GEN_DIR)/*.cpp)
GEN_HDR := $(wildcard $(GEN_DIR)/*.h)
GEN_OBJ := $(patsubst $(GEN_DIR)/%.cpp,$(OBJ)/%.o,$(GEN_SRC))

# Util Sources
UTL_SRC := $(wildcard $(UTL_DIR)/*.cpp)
UTL_BIN := $(patsubst $(UTL_DIR)/%.cpp,$(BIN)/%,$(UTL_SRC))

# Default
all: dir $(GEN_OBJ) $(UTL_BIN) $(EXT_TARS)

# Object directory
dir:
	test -d $(OBJ) || mkdir $(OBJ)
	test -d $(BIN) || mkdir $(BIN)
# Clean
clean: $(CLN)
	rm -rf $(OBJ)
	rm -f $(UTL_BIN)

# Compile Common Sources
$(OBJ)/%.o: $(GEN_DIR)/%.cpp $(GEN_DIR)/%.h
	$(CC) -c $(CFLAGS) $(DEF) -o $@ $<

# Comile utilities
$(BIN)/%: $(UTL_DIR)/%.cpp $(GEN_OBJ) $(LOC_OBJ) $(DEV_OBJ)
	$(CC) $(CFLAGS) $(DEF) $(OBJ)/* -o $@ $< $(LFLAGS) 

