#!/bin/sh

# Switch to C-shell
chsh -s /bin/tcsh

# Make the "host" directory
mkdir -p /mnt/host

# NFS mount the source code
mount -t nfs 192.168.2.1:/u1/ExampleRceProject/software/ /mnt/host

# remove old driver
rmmod -s AxiStreamDmaModule

# add new driver
insmod /mnt/host/Common/AxiStreamDmaModule.ko cfgRxSize=131072,0,0,0 cfgRxCount=32,0,0,0 cfgTxSize=131072,0,0,0 cfgTxCount=32,0,0,0 cfgRxAcp=0,0,0,0

# give appropriate group/permissions
chmod a+rw /dev/axi*
