//-----------------------------------------------------------------------------
// File          : PrbsData.h
// Author        : Ryan Herbst <rherbst@slac.stanford.edu>
// Created       : 10/27/2014
// Project       : LBNE DAQ
//-----------------------------------------------------------------------------
// Description :
// Process blocks of PRBS data received from DMA engine. Used to verify
// DMA operation and memory mapping.
//-----------------------------------------------------------------------------
// This file is part of 'Example RCE Project'.
// It is subject to the license terms in the LICENSE.txt file found in the 
// top-level directory of this distribution and at: 
//    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
// No part of 'Example RCE Project', including this file, 
// may be copied, modified, propagated, or distributed except according to 
// the terms contained in the LICENSE.txt file.
// Proprietary and confidential to SLAC.
//-----------------------------------------------------------------------------
// Modification history :
// 10/27/2014: created
//-----------------------------------------------------------------------------
#ifndef __PRBS_DATA_H__
#define __PRBS_DATA_H__
#include <stdint.h>

// Main Class
class PrbsData {

      uint32_t * _taps;
      uint32_t   _tapCnt;
      uint32_t   _width;
      uint32_t   _sequence;

      uint32_t flfsr(uint32_t input);

   public:

      PrbsData(uint32_t width, uint32_t tapCnt, ... );
      ~PrbsData();

      bool processData ( const void *data, uint32_t size );

};

#endif


